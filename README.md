The following files needs to be modified to get a functioning PTP master using an EVR

```
/opt/iocs/utg-evr-01/st.cmd
/etc/chrony.conf*
/etc/ptp4l.conf
/etc/sysconfig/phc2sys
/etc/sysconfig/ptp4l
```
For a PTP server , set this in ptp4l.conf:

slave_only: 0 
master_prio: 127 
 

For a PTP slave machine, set this in ptp4l.conf:

slave_only: 1 
master_prio: 128 
And this in phc2sys

OPTIONS="-a -r" 
and also another IP address than 192.168.3.1 must be set, to avoid conflicts...
